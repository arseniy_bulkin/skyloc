# Web-assistant

### Перейти в папку frontend
```cd frontend```

### Установить nodejs и gulp
1. http://nodejs.org/
2. ```npm install -g gulp```

### Установить зависимости
```npm install```

### Установить jquery
```bower install```

### Сборка
```gulp``` - собирает проект